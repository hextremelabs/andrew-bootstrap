package com.hextremelabs.andrewbootstrap.ws;

import com.hextremelabs.andrewbootstrap.business.DvdHandler;
import com.hextremelabs.andrewbootstrap.model.Dvd;

import javax.ejb.EJB;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@WebService(name = "Endpoint")
public class SoapEndpoint {

  @EJB
  private DvdHandler handler;

  @WebMethod
  public Dvd getDvd(@WebParam(name = "id") Long id) {
    return handler.getDvd(id);
  }

  @WebMethod
  public List<Dvd> findAll() {
    return handler.findAll();
  }

  @WebMethod
  public Dvd addDvd(@WebParam(name = "dvd") Dvd dvd) {
    return handler.addDvd(dvd);
  }

  @WebMethod
  public Dvd update(@NotNull @Valid @WebParam(name = "dvd") Dvd dvd) {
    return handler.update(dvd);
  }

  @WebMethod
  public List<Dvd> searchByTitle(@WebParam(name = "title") String title) {
    return handler.searchByTitle(title);
  }

  @WebMethod
  public Dvd delete(@WebParam(name = "id") Long id) {
    return handler.delete(id);
  }

  @WebMethod
  public void setRented(@WebParam(name = "id") Long id, @WebParam(name = "value") Boolean value) {
    handler.setRented(id, value);
  }

  @WebMethod
  public void setAvailability(@WebParam(name = "id") Long id, @WebParam(name = "value") Boolean value) {
    handler.setAvailability(id, value);
  }
}
