package com.hextremelabs.andrewbootstrap.business;

import com.hextremelabs.andrewbootstrap.model.Dvd;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Startup
@Singleton
public class Bootstrap {

  @PersistenceContext
  private EntityManager em;

  @PostConstruct
  public void init() {
    final Dvd spiderman = new Dvd();
    spiderman.setTitle("Spiderman - Home Coming");
    spiderman.setAvailable(true);
    spiderman.setRented(true);

    final Dvd theLastWord = new Dvd();
    theLastWord.setTitle("The Last Word");
    theLastWord.setAvailable(false);
    theLastWord.setRented(true);

    final Dvd amazingSpiderman = new Dvd();
    amazingSpiderman.setTitle("Amazing Spiderman");
    amazingSpiderman.setAvailable(true);
    amazingSpiderman.setRented(false);

    final Dvd missSloane = new Dvd();
    missSloane.setTitle("Miss Sloane");
    missSloane.setAvailable(false);
    missSloane.setRented(false);

    em.persist(spiderman);
    em.persist(theLastWord);
    em.persist(amazingSpiderman);
    em.persist(missSloane);
  }
}
