package com.hextremelabs.andrewbootstrap.business;

import com.hextremelabs.andrewbootstrap.model.Dvd;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author Andrew
 */
@Stateless
public class DvdHandler {

  @PersistenceContext
  private EntityManager em;

  public Dvd addDvd(Dvd dvd) {
    Dvd newDvd = new Dvd();
    newDvd.setTitle(dvd.getTitle());
    newDvd.setAvailable(dvd.getAvailable());
    newDvd.setRented(dvd.getRented());
    em.persist(newDvd);
    return newDvd;
  }

  public Dvd getDvd(Long id) {
    return em.find(Dvd.class, id);
  }

  public List<Dvd> findAll() {
    return em.createQuery("SELECT d FROM Dvd d").getResultList();
  }

  public List<Dvd> searchByTitle(String title) {
    return em.createQuery("SELECT d from Dvd d WHERE d.title LIKE :title")
        .setParameter("title", "%" + title + "%")
        .getResultList();
  }

  public Dvd delete(Long id) {
    final Dvd dvd = getDvd(id);
    em.remove(dvd);
    return dvd;
  }

  public Dvd update(Dvd dvd) {
    em.merge(dvd);
    return dvd;
  }

  public void setRented(Long id, boolean value) {
    Dvd dvd = em.find(Dvd.class, id);
    dvd.setRented(value);
  }

  public void setAvailability(Long id, boolean value) {
    Dvd dvd = em.find(Dvd.class, id);
    dvd.setAvailable(value);
  }
}
