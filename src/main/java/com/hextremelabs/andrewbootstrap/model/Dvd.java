package com.hextremelabs.andrewbootstrap.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author Andrew
 */
@Entity
@XmlRootElement
public class Dvd implements Serializable {

  private static final long serialVersionUID = -5584404843358199527L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(nullable = false)
  private Long id;

  @NotNull(message = "Title cannot be null")
  @Size(min = 1, max = 255, message = "Title must be between ${min} and ${max}")
  @Column(name = "title", nullable = false)
  private String title;

  private Boolean available;
  private Boolean rented;

  public Dvd() {
  }

  public Dvd(Long id, String title, Boolean available, Boolean rented) {
    this.id = id;
    this.title = title;
    this.available = available;
    this.rented = rented;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Boolean getAvailable() {
    return available;
  }

  public void setAvailable(Boolean available) {
    this.available = available;
  }

  public Boolean getRented() {
    return rented;
  }

  public void setRented(Boolean rented) {
    this.rented = rented;
  }

  @Override
  public String toString() {
    return "Dvd{" +
        "id=" + id +
        ", title='" + title + '\'' +
        ", available=" + available +
        ", rented=" + rented +
        '}';
  }
}
