package com.hextremelabs.andrewbootstrap.rpc;

import com.hextremelabs.andrewbootstrap.business.DvdHandler;
import com.hextremelabs.andrewbootstrap.model.Dvd;

import javax.ejb.EJB;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author Andrew
 */
@Path("dvd")
public class RestEndpoint {

  @EJB
  private DvdHandler handler;

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<Dvd> getAllDvd() {
    return handler.findAll();
  }

  @GET
  @Path("/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Dvd getDvd(@PathParam("id") Long id) {
    return handler.getDvd(id);
  }

  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public Dvd addDvd(Dvd dvd) {
    return handler.addDvd(dvd);
  }

  @PUT
  @Produces(MediaType.APPLICATION_JSON)
  public Dvd update(@NotNull @Valid Dvd dvd) {
    return handler.update(dvd);
  }

  @GET
  @Path("/search")
  @Produces(MediaType.APPLICATION_JSON)
  public List<Dvd> searchDvd(@QueryParam("title") String title) {
    return handler.searchByTitle(title);
  }

  @DELETE
  @Path("/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Dvd deleteDvd(@PathParam("id") Long id) {
    return handler.delete(id);
  }

  @PUT
  @Path("/rent/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response setRented(@PathParam("id") Long id, @QueryParam("rented") @DefaultValue("false") Boolean value) {
    handler.setRented(id, value);
    return Response.ok().build();
  }

  @PUT
  @Path("/available/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response setAvailability(@PathParam("id") Long id, @QueryParam("available") @DefaultValue("false") Boolean value) {
    handler.setAvailability(id, value);
    return Response.ok().build();
  }
}
