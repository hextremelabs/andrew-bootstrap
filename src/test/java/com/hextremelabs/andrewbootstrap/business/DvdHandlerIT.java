package com.hextremelabs.andrewbootstrap.business;

import com.hextremelabs.andrewbootstrap.model.Dvd;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ejb.EJB;
import java.util.List;

@RunWith(Arquillian.class)
public class DvdHandlerIT {

  @EJB
  private DvdHandler dvdHandler;

  @Deployment
  public static Archive<?> createDeployment() {
    return ShrinkWrap.create(JavaArchive.class)
        .addClasses(Dvd.class, DvdHandler.class)
        .addAsManifestResource("persistence.xml")
        .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
  }


  @Test
  public void testCanPersistAndSearchDvd() {
    final Dvd spiderman = new Dvd();
    spiderman.setTitle("Spiderman - Home Coming");
    spiderman.setAvailable(true);
    spiderman.setRented(true);

    final Dvd theLastWord = new Dvd();
    theLastWord.setTitle("The Last Word");
    theLastWord.setAvailable(false);
    theLastWord.setRented(true);

    dvdHandler.addDvd(spiderman);
    dvdHandler.addDvd(theLastWord);

    List<Dvd> dvdList = dvdHandler.searchByTitle("Home");

    Assert.assertNotNull(dvdList);
    Assert.assertTrue(dvdList.size() == 1);
    Assert.assertEquals(dvdList.get(0).getTitle(), "Spiderman - Home Coming");
    Assert.assertEquals(dvdList.get(0).getAvailable(), true);
    Assert.assertEquals(dvdList.get(0).getRented(), true);
  }

  @Test
  public void testCanUpdateDvd(){
    final Dvd updatedDvd = new Dvd();
    updatedDvd.setId(1L);
    updatedDvd.setTitle("Bat Man and The Joker");
    updatedDvd.setAvailable(false);
    updatedDvd.setRented(false);
    dvdHandler.update(updatedDvd);

    Dvd dvd = dvdHandler.getDvd(1L);

    Assert.assertNotNull(dvd);
    Assert.assertEquals(dvd.getTitle(), "Bat Man and The Joker");
    Assert.assertEquals(dvd.getAvailable(), false);
    Assert.assertEquals(dvd.getRented(), false);
  }
}
