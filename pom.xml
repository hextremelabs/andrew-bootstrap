<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

  <modelVersion>4.0.0</modelVersion>

  <groupId>com.hextremelabs.andrewbootstrap</groupId>
  <artifactId>dvd-rental</artifactId>
  <version>1.0-SNAPSHOT</version>
  <packaging>war</packaging>

  <name>${project.artifactId}</name>

  <properties>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
    <failOnMissingWebXml>false</failOnMissingWebXml>

    <version.arquillian>1.1.8.Final</version.arquillian>
    <version.wildfly>8.2.0.Final</version.wildfly>

    <mysql.driver.version>5.1.39</mysql.driver.version>
    <db.driver.name>mysql-connector-java-${mysql.driver.version}-${project.artifactId}</db.driver.name>
    <datasource.name>andrewbootstrap</datasource.name>
    <db.name>andrewbootstrap</db.name>
    <!-- Leave blank to not use SSL. Would be passed as environment variable from CI server if SSL is desired. -->
    <db.ssl></db.ssl>
  </properties>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>org.jboss.arquillian</groupId>
        <artifactId>arquillian-bom</artifactId>
        <version>${version.arquillian}</version>
        <scope>import</scope>
        <type>pom</type>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <dependencies>
    <!--Provided Scope-->
    <dependency>
      <groupId>javax</groupId>
      <artifactId>javaee-api</artifactId>
      <version>7.0</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>mysql</groupId>
      <artifactId>mysql-connector-java</artifactId>
      <version>5.1.39</version>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.11</version>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>org.jboss.arquillian.junit</groupId>
      <artifactId>arquillian-junit-container</artifactId>
    </dependency>
  </dependencies>

  <scm>
    <connection>scm:git:https://bitbucket.org/hextremelabs/andrew-bootstrap.git</connection>
    <url>https://bitbucket.org/hextremelabs/andrew-bootstrap.git</url>
  </scm>

  <profiles>
    <profile>
      <!-- The profile to be used for regualr build lifecycle - compilation, package building, unit tests etc.
      The default profile doesn't execute integration tests-->
      <id>default</id>
      <activation>
        <activeByDefault>true</activeByDefault>
      </activation>
      <build>
        <finalName>${project.artifactId}-${project.version}-${scmBranch}-${buildNumber}</finalName>
        <plugins>
          <plugin>
            <artifactId>maven-surefire-plugin</artifactId>
            <version>2.16</version>
            <configuration>
              <skip>true</skip>
            </configuration>
          </plugin>

          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-war-plugin</artifactId>
            <configuration>
              <webResources>
                <resource>
                  <directory>src/main/resources</directory>
                  <targetPath>META-INF</targetPath>
                  <includes>
                    <include>logging.properties</include>
                  </includes>
                </resource>
              </webResources>
            </configuration>
          </plugin>

          <plugin>
            <groupId>com.sebastian-daschner</groupId>
            <artifactId>jaxrs-analyzer-maven-plugin</artifactId>
            <version>0.14</version>
            <dependencies>
              <dependency>
                <groupId>javax</groupId>
                <artifactId>javaee-api</artifactId>
                <version>7.0</version>
              </dependency>
            </dependencies>
            <executions>
              <execution>
                <id>generate-plaintext</id>
                <goals>
                  <goal>analyze-jaxrs</goal>
                </goals>
                <configuration>
                  <backend>plaintext</backend>
                  <deployedDomain>dev.hextremelabs.net/AndrewBootstrap</deployedDomain>
                </configuration>
              </execution>
              <execution>
                <id>generate-asciidoc</id>
                <goals>
                  <goal>analyze-jaxrs</goal>
                </goals>
                <configuration>
                  <backend>asciidoc</backend>
                  <deployedDomain>dev.hextremelabs.net/AndrewBootstrap</deployedDomain>
                </configuration>
              </execution>
            </executions>
          </plugin>

          <plugin>
            <groupId>org.asciidoctor</groupId>
            <artifactId>asciidoctor-maven-plugin</artifactId>
            <version>1.5.5</version>
            <dependencies>
              <dependency>
                <groupId>org.asciidoctor</groupId>
                <artifactId>asciidoctorj-pdf</artifactId>
                <version>1.5.0-alpha.16</version>
              </dependency>
            </dependencies>
            <executions>
              <execution>
                <id>generate-pdf-doc</id>
                <phase>process-classes</phase>
                <goals>
                  <goal>process-asciidoc</goal>
                </goals>
                <configuration>
                  <title>REST API Doc for Lite</title>
                  <backend>pdf</backend>
                  <sourceDirectory>${basedir}/target/jaxrs-analyzer</sourceDirectory>
                  <sourceDocumentName>rest-resources.adoc</sourceDocumentName>
                  <sourceHighlighter>rouge</sourceHighlighter>
                  <attributes>
                    <icons>font</icons>
                    <pagenums />
                    <toc />
                    <idprefix />
                    <idseparator>-</idseparator>
                  </attributes>
                </configuration>
              </execution>
            </executions>
          </plugin>

          <plugin>
            <groupId>org.wildfly.plugins</groupId>
            <artifactId>wildfly-maven-plugin</artifactId>
            <version>1.1.0.Final</version>

            <configuration>
              <filename>${project.artifactId}-${project.version}-${scmBranch}-${buildNumber}.${project.packaging}</filename>
              <name>${project.artifactId}-${project.version}-${scmBranch}-${buildNumber}.${project.packaging}</name>

              <hostname>${wildfly.host}</hostname>
              <port>${wildfly.port}</port>
              <username>${wildfly.user}</username>
              <password>${wildfly.password}</password>
              <serverConfig>standalone.xml</serverConfig>
            </configuration>

            <executions>
              <execution>
                <id>undeploy</id>
                <phase>install</phase>
                <goals>
                  <goal>undeploy</goal>
                </goals>
                <configuration>
                  <matchPattern>${project.artifactId}.*</matchPattern>
                  <ignoreMissingDeployment>true</ignoreMissingDeployment>
                </configuration>
              </execution>

              <execution>
                <id>deploy-driver</id>
                <phase>install</phase>
                <goals>
                  <goal>deploy-artifact</goal>
                </goals>
                <configuration>
                  <groupId>mysql</groupId>
                  <artifactId>mysql-connector-java</artifactId>
                  <name>${db.driver.name}</name>
                </configuration>
              </execution>

              <execution>
                <id>add-datasource</id>
                <phase>install</phase>
                <goals>
                  <goal>execute-commands</goal>
                </goals>
                <configuration>
                  <fail-on-error>false</fail-on-error>
                  <commands>
                    <command>
                      data-source add
                      --name=${datasource.name}
                      --jndi-name=java:jboss/datasources/${datasource.name}
                      --driver-name=${db.driver.name}_com.mysql.jdbc.Driver_5_1
                      --driver-class=com.mysql.jdbc.Driver
                      --connection-url=jdbc:mysql://${db.address}/${db.name}?autoReconnect=true&amp;characterEncoding=utf8&amp;zeroDateTimeBehavior=convertToNull${db.ssl}
                      --new-connection-sql="SET NAMES 'utf8mb4'"
                      --user-name=${db.user}
                      --password=${db.password}
                      --enabled=true
                      --use-ccm=false
                      --jta=true
                      --validate-on-match=false
                      --background-validation=false
                      --share-prepared-statements=false
                    </command>
                  </commands>
                </configuration>
              </execution>

              <execution>
                <id>suppress-applog-in-syslog</id>
                <phase>install</phase>
                <goals>
                  <goal>execute-commands</goal>
                </goals>
                <configuration>
                  <fail-on-error>false</fail-on-error>
                  <commands>
                    <command>/subsystem=logging/logger=com.hextremelabs:add</command>
                    <command>/subsystem=logging/logger=com.hextremelabs:write-attribute(name=level,value=FATAL)</command>
                  </commands>
                </configuration>
              </execution>

              <execution>
                <id>deploy</id>
                <phase>install</phase>
                <goals>
                  <goal>deploy</goal>
                </goals>
              </execution>
            </executions>
          </plugin>

          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-antrun-plugin</artifactId>
            <version>1.7</version>
            <executions>
              <execution>
                <id>server-copy</id>
                <goals>
                  <goal>run</goal>
                </goals>
                <phase>install</phase>
                <configuration>
                  <target>
                    <scp trust="yes"
                         file="${basedir}/target/generated-docs/rest-resources.pdf"
                         todir="${wildfly.user}:${wildfly.password}@${wildfly.host}:/opt/wildfly/welcome-content/"
                         failonerror="false"/>
                  </target>
                </configuration>
              </execution>
            </executions>
            <dependencies>
              <dependency>
                <groupId>org.apache.ant</groupId>
                <artifactId>ant-jsch</artifactId>
                <version>1.8.2</version>
              </dependency>
            </dependencies>
          </plugin>

          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>buildnumber-maven-plugin</artifactId>
            <version>1.4</version>
            <executions>
              <execution>
                <phase>validate</phase>
                <goals>
                  <goal>create</goal>
                </goals>
              </execution>
            </executions>
            <configuration>
              <doCheck>false</doCheck>
              <doUpdate>false</doUpdate>
              <shortRevisionLength>6</shortRevisionLength>
            </configuration>
          </plugin>
        </plugins>
      </build>
    </profile>

    <profile>
      <!-- The profile to execute wildfly managed integration tests.
      Other profiles similar to this could be created for other container adapters-->
      <id>wildfly-managed</id>
      <!-- the dependencies for Wildfly  -->
      <dependencies>
        <dependency>
          <groupId>org.wildfly</groupId>
          <artifactId>wildfly-arquillian-container-managed</artifactId>
          <version>8.2.1.Final</version>
        </dependency>
        <dependency>
          <groupId>org.wildfly</groupId>
          <artifactId>wildfly-embedded</artifactId>
          <version>8.2.0.Final</version>
        </dependency>
      </dependencies>

      <build>
        <plugins>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-dependency-plugin</artifactId>
            <version>2.8</version>
            <executions>
              <execution>
                <id>unpack</id>
                <phase>process-test-classes</phase>
                <goals>
                  <goal>unpack</goal>
                </goals>
                <configuration>
                  <artifactItems>
                    <artifactItem>
                      <groupId>org.wildfly</groupId>
                      <artifactId>wildfly-dist</artifactId>
                      <version>${version.wildfly}</version>
                      <type>zip</type>
                      <overWrite>false</overWrite>
                      <outputDirectory>target</outputDirectory>
                    </artifactItem>
                  </artifactItems>
                </configuration>
              </execution>
            </executions>
          </plugin>

          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-failsafe-plugin</artifactId>
            <version>2.18.1</version>
            <executions>
              <execution>
                <goals>
                  <goal>integration-test</goal>
                  <goal>verify</goal>
                </goals>
                <configuration>
                  <systemPropertyVariables>
                    <arquillian.launch>wildfly-managed</arquillian.launch>
                    <java.util.logging.manager>org.jboss.logmanager.LogManager</java.util.logging.manager>
                    <jboss.home>${project.basedir}/target/wildfly-8.2.0.Final</jboss.home>
                    <module.path>${project.basedir}/target/wildfly-8.2.0.Final/modules</module.path>
                  </systemPropertyVariables>
                  <redirectTestOutputToFile>false</redirectTestOutputToFile>
                </configuration>
              </execution>
            </executions>
          </plugin>

          <plugin>
            <groupId>org.jacoco</groupId>
            <artifactId>jacoco-maven-plugin</artifactId>
            <version>0.7.7.201606060606</version>
            <configuration>
              <destFile>${basedir}/target/coverage-reports/jacoco-unit.exec</destFile>
              <dataFile>${basedir}/target/coverage-reports/jacoco-unit.exec</dataFile>
            </configuration>
            <executions>
              <execution>
                <id>jacoco-initialize</id>
                <goals>
                  <goal>prepare-agent</goal>
                </goals>
              </execution>
              <execution>
                <id>jacoco-site</id>
                <phase>package</phase>
                <goals>
                  <goal>report</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>
</project>
